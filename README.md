# Puzzle
Puzzle is going to be a content management system (CMS) with a highly modular front end. 
The main goal is having an easy-to-use homepage editor with a toolbox consisting of premade and self-made controls called *puzzle pieces*.
