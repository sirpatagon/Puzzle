﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Puzzle.Data
{
    public enum CRUDOperations
    {
        Create = 0,
        Read = 1,
        Update = 2,
        Delete = 3,
    }
}
